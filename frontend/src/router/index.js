import Home from "@/views/Home.vue";
import Games from "@/views/Games.vue";
import { createRouter, createWebHistory } from "vue-router";
const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/games",
        name: "Games",
        component: Games,
    },
];

export default createRouter({
    history: createWebHistory(),
    routes
});
