import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/css/main.css';
import Vuex from 'vuex'
import store from './store'

const vuex = new Vuex.Store(store);
createApp(App).use(router).use(vuex).use(store).mount('#app')
