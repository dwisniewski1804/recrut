import axios from 'axios';

export default {
    getGamesList (filters) {
        const params = filters ? {
            countryCode: filters. countryCode,
            categoryId: filters. categoryId,
            brandId: filters. brandId,
        }: null;
        return axios.get(`/api/games`, {
            params: params,
        })
    },
    getCategoriesList () {
        return axios.get(`/api/games/categories`)
    },
    getBrandList() {
        return axios.get(`/api/games/brands`)
    },
    getCountriesList() {
        return axios.get(`/api/countries`)
    }
}