import gamesAPI from "@/api/Games/games";

//to handle state
const state = {
    brands: [],
    categories: [],
    countries: [],
}

//to handle state
const getters = {
    allBrands: (state) => state.brands,
    allCategories: (state) => state.categories,
    allCountries: (state) => state.countries
}

//to handle actions
const actions = {
    getBrands({ commit }) {
        gamesAPI.getBrandList().then(response => {
            commit('SET_BRANDS', response.data)
        });
    },
    getCountries({ commit }) {
        gamesAPI.getCountriesList().then(response => {
            commit('SET_COUNTRIES', response.data)
        });
    },
    getCategories({ commit }) {
        gamesAPI.getCategoriesList().then(response => {
            commit('SET_CATEGORIES', response.data)
        });
    },
}

//to handle mutations
const mutations = {
    SET_BRANDS(state, brands) {
        state.brands = brands
    },
    SET_CATEGORIES(state, categories) {
        state.categories = categories
    },
    SET_COUNTRIES(state, countries) {
        state.countries = countries
    }
}

//export store module
export default {
    state,
    getters,
    actions,
    mutations
}
/** we have just created a boiler plate for our vuex store module**/