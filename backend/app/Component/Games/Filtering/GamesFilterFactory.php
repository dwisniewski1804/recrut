<?php
declare(strict_types=1);
namespace App\Component\Games\Filtering;


use Illuminate\Http\Request;

/**
 * Class GamesFilterFactory
 * @package App\Component\Games\Filtering
 */
class GamesFilterFactory
{
    private GamesFilterInterface $gamesFilter;
    /**
     * GamesFilterFactory constructor.
     * @param Request $request
     */
    public function __construct(Request $request) {
        $brandId = (int)$request->get('brandId');
        $countryCode = $request->get('countryCode');
        $categoryId = (int)$request->get('categoryId');

        $this->gamesFilter = new GamesFilter($brandId, $countryCode, $categoryId);
    }

    /**
     * @return GamesFilterInterface
     */
    public function getFilter(): GamesFilterInterface {
        return $this->gamesFilter;
    }
}
