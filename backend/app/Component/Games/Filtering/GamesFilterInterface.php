<?php
declare(strict_types=1);

namespace App\Component\Games\Filtering;

use Illuminate\Database\Query\Builder;

/**
 * Interface GamesFilterInterface
 * @package App\Component\Games\Filtering
 */
interface GamesFilterInterface
{
    /**
     * GamesFilterInterface constructor.
     * @param int|null    $brandId
     * @param string|null $countryCode
     * @param int         $categoryId
     */
    public function __construct(?int $brandId, ?string $countryCode, int $categoryId);

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function applyFilters(Builder $builder): Builder;
}
