<?php
declare(strict_types=1);

namespace App\Component\Games\Filtering;

use App\Model\BrandGame;
use App\Model\Game;
use App\Model\GameBrandBlock;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class GamesFilter
 * @package App\Component\Games\Filtering
 */
class GamesFilter implements GamesFilterInterface
{
    private array $filtersToApply = [];
    private const BRAND = 'brand';
    private const CATEGORY = 'category';
    private const COUNTRY = 'country';

    /**
     * GamesFilter constructor.
     * @param int|null    $brandId
     * @param string|null $countryCode
     * @param int         $categoryId
     */
    public function __construct(?int $brandId, ?string $countryCode, int $categoryId)
    {
        if($brandId) {
            $this->filtersToApply[self::BRAND] = $brandId;
        }
        if($countryCode) {
            $this->filtersToApply[self::COUNTRY] = $countryCode;
        }
        if($categoryId) {
            $this->filtersToApply[self::CATEGORY] = $categoryId;
        }
    }

    public function applyFilters(Builder $builder): Builder
    {
        foreach ($this->filtersToApply as $name => $filterParam) {
            switch ($name) {
                case self::BRAND:
                    $builder->where(BrandGame::TABLE.'.brandid', '=', $filterParam)
                        ->whereNotIn(Game::TABLE.'.launchcode',
                            DB::table(Game::TABLE)
                                ->select(Game::TABLE.'.launchcode')
                                ->join(GameBrandBlock::TABLE, Game::TABLE.'.launchcode', '=', GameBrandBlock::TABLE.'.launchcode')
                                ->where(GameBrandBlock::TABLE.'.brandid', '=', $filterParam)
                        );
                    break;
                case self::COUNTRY:
                    // TODO apply filter for country
                    break;
                case self::CATEGORY:
                    // TODO apply filter to category
                    break;
            }
        }

        return $builder;
    }
}
