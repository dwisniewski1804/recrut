<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Brand;
use Illuminate\Support\Facades\DB;

/**
 * Class GamesRepository
 * @package App\Repository
 */
class BrandsRepository
{
    public function listAll(): array
    {
        return DB::table(Brand::TABLE)->get()->toArray();
    }
}
