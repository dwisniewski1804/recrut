<?php
declare(strict_types=1);

namespace App\Repository;

use App\Component\Games\Filtering\GamesFilterFactory;
use App\Model\Brand;
use App\Model\BrandGame;
use App\Model\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class GamesRepository
 * @package App\Repository
 */
class GamesRepository
{
    /**
     * @param Request $request
     * @return array
     */
    public function getGames(Request $request): array
    {
        $filtersFactory = new GamesFilterFactory($request);
        $filter = $filtersFactory->getFilter();
        // TOOD new hot w tabeli game/brand
        $qb = DB::table(Game::TABLE)
            ->distinct(Game::TABLE.'.launchcode')
            ->select(Game::TABLE.'.launchcode', Game::TABLE.'.name', Game::TABLE.'.rtp', Game::TABLE.'.provider')
            ->join(BrandGame::TABLE, Game::TABLE.'.launchcode', '=', BrandGame::TABLE.'.launchcode')
            ->join(Brand::TABLE, BrandGame::TABLE.'.brandid', '=', Brand::TABLE.'.id')
            ->limit($request->get('limit') ?? 10);
        $qb = $filter->applyFilters($qb);

        return $qb->get()->toArray();
    }
}
