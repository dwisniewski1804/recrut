<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BrandGame
 * @package App\Model
 */
class BrandGame extends Model
{
    const TABLE = 'brand_games';
    protected $table = self::TABLE;
}
