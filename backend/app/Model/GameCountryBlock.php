<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GameCountryBlock
 * @package App\Model
 */
class GameCountryBlock extends Model
{
    const TABLE = 'game_country_block';
    protected $table = self::TABLE;
}
