<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Game
 * @package App\Model
 */
class Game extends Model
{
    const TABLE = 'game';
    protected $table = self::TABLE;
}
