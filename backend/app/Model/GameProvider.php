<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GameProvider
 * @package App\Model
 */
class GameProvider extends Model
{
    const TABLE = 'game_providers';
    protected $table = self::TABLE;
}
