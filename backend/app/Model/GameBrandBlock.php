<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GameBrandBlock
 * @package App\Model
 */
class GameBrandBlock extends Model
{
    const TABLE = 'game_brand_block';
    protected $table = self::TABLE;
}
