<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BrandGame
 * @package App\Model
 */
class Brand extends Model
{
    const TABLE = 'brands';
    protected $table = self::TABLE;
}
