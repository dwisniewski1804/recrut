<?php
declare(strict_types=1);

namespace App\Http\Controllers;


use App\Repository\BrandsRepository;
use Illuminate\Http\JsonResponse;

/**
 * Class GamesController
 * @package App\Http\Controllers
 */
class BrandsController extends Controller
{
    public function __construct(private BrandsRepository $gamesRepository){}

    public function list()
    {
        $games = $this->gamesRepository->listAll();
        return new JsonResponse($games);
    }
}
