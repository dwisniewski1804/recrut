<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repository\GamesRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class GamesController
 * @package App\Http\Controllers
 */
class GamesController extends Controller
{
    public function __construct(private GamesRepository $gamesRepository){}

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        try {
            $games = $this->gamesRepository->getGames($request);
        }
        catch (Exception) {
            return new JsonResponse('Bad response, check logs to find out more.', JsonResponse::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($games);
    }
}
